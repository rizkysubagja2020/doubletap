# Langkah Awal
## 1. build.gradle(project)
### tambahkan "maven { url 'https://jitpack.io' }" di allproject
````
allprojects {
    repositories {
        google()
        jcenter()
        //
        maven { url 'https://jitpack.io' }
    }
}
````
## 2. build.gradle(app)
### tambahkan compileOption, kemudian tambahkan ExoPlayer dan Library DoubleTap di dependencies
````
//
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

dependencies {
    // ExoPlayer2
    implementation "com.google.android.exoplayer:exoplayer-core:2.11.7"
    implementation "com.google.android.exoplayer:exoplayer-ui:2.11.7"
    // material
    implementation 'com.google.android.material:material:1.2.1'
    implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'

    // Library DoubleTap
    implementation 'com.github.vkay94:DoubleTapPlayerView:1.0.0'
}

````
## 3. Tambahkan layout 
### activty_main.xml, exo_playback_contro_view_yt.xml, yt_overlay.xml, yt_second_view
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/res/layout/activity_main.xml)
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/res/layout/exo_playback_control_view_yt.xml)
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/res/layout/yt_overlay.xml)
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/res/layout/yt_seconds_view.xml)

## 4. Tambahkan Data.kt, Mp4ExtratorFactory.kt, BaseVideoActivity.kt dan MainActivity.kt
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/java/com/example/learndoubletap/Data.kt)
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/java/com/example/learndoubletap/Mp4ExtractorFactory.kt)
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/java/com/example/learndoubletap/BaseVideoActivity.kt)
(https://gitlab.com/rizkysubagja2020/doubletap/-/blob/master/app/src/main/java/com/example/learndoubletap/MainActivity.kt)

