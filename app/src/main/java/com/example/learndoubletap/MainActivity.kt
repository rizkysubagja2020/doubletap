package com.example.learndoubletap

import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.View
import com.github.vkay94.dtpv.youtube.YouTubeOverlay
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.exo_playback_control_view_yt.*

class MainActivity : BaseVideoActivity() {

    // set variable FullScreen and Video
    private var isVideoFullScreen = false
    private var currentVideoId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.videoPlayer = previewPlayerView

        // initiation
        initDoubleTapPlayer()
        startNextVideo()

        // set button fullscreen
        fullscreen_button.setOnClickListener {
            toggleFullScreen()
        }

    }

    // DoubleTapPlayer
    private fun initDoubleTapPlayer() {
        ytOverlay
            .performListener(object : YouTubeOverlay.PerformListener {
                override fun onAnimationStart() {
                    previewPlayerView.useController = false
                    ytOverlay.visibility = View.VISIBLE
                }
                override fun onAnimationEnd() {
                    ytOverlay.visibility = View.GONE
                    previewPlayerView.useController = true
                }
            })

        previewPlayerView.doubleTapDelay = 800
    }

    // Start Video
    private fun startNextVideo() {
        releasePlayer()
        initializerPlayer()
        ytOverlay.player(player!!)

        currentVideoId = (currentVideoId + 1).rem(Data.videoList.size)
        buildMediaSource(Uri.parse(Data.videoList[currentVideoId]))
    }

    // FullScreen
    private fun toggleFullScreen() {
        if (isVideoFullScreen) {
            setFullScreen(false)
            window.decorView.visibility = View.VISIBLE
            if (supportActionBar != null) {
                supportActionBar?.show()
            }
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            isVideoFullScreen = false
        } else {
            setFullScreen(true)
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                    and View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    and View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
        }
        if(supportActionBar != null){
            supportActionBar?.hide()
        }
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        isVideoFullScreen = true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    // Back from fullscreen
    override fun onBackPressed() {
        if (isVideoFullScreen) {
            toggleFullScreen()
            return
        }
        super.onBackPressed()
    }
}